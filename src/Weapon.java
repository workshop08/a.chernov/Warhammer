//TODO: Сделать строки полями-константами и через enum
//TODO: Больше информации в print, разбить на несколько строк
//TODO: Сделать класс Item и наследовать Weapon и Shield от него
//TODO: Если хватает фантазии, желательно больше характеристик
public class Weapon
{
    private int damage, id;
    private String name;
    private static int i = 0;

    public Weapon(String name, int id, int damage)
    {
        this.damage = damage;
        this.name = name;
        this.id = id;
        i++;
    }

    public void arPrint()
    {
        System.out.println(this.name + this.damage + this.id + ".\n");
    }

    public int getId()
    {
        return this.id;
    }

    public void setDamage(int a)
    {
        this.damage = a;
    }

    public int getDamage()
    {
        return this.damage;
    }

    public void setName(String x)
    {
        this.name = x;
    }

    public String getName()
    {
        return this.name;
    }
}
