package Units;

public class Swordman extends Unit
{
    public Swordman(String name, int speed, int damage, int health)
    {
        this.unitname = name;
        this.speed = speed;
        this.health = health;
        this.damage = damage;
        this.id = id;
        id++;
    }
}