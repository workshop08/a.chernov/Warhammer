package Units;

public class Archer extends Unit
{
    public Archer(String name, int speed, int damage, int health)
    {
        this.unitname = name;
        this.speed = speed;
        this.health = health;
        this.damage = damage;
        this.id = id;
        id++;
    }
}
