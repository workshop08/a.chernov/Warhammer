package Items;

public abstract class item
{
    protected String name, type;
    public int health;
    public int damage;
    public int weight;
    public int id = 0;

    public void setWeight(int x)
    {
        this.weight = x;
    }

    public int getWeight()
    {
        return this.weight;
    }

    public void setHealth(int x)
    {
        this.health = x;
    }

    public int getHealth()
    {
        return this.health;
    }

    public void setType(String t)
    {
        this.type = t;
    }

    public String getType()
    {
        return type;
    }

    public void setName(String a)
    {
        this.name = a;
    }

    public String getName()
    {
        return this.name;
    }

    public int getId()
    {
        return this.id;
    }

    public void setDamage(int x)
    {
        this.damage = x;
    }

    public int getDamage()
    {
        return this.damage;
    }
}