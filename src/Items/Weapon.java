package Items;

public class Weapon extends item
{
    public int range;

    public Weapon(String name, int damage, int weight, int range)
    {
        this.damage = damage;
        this.weight = weight;
        this.name = name;
        this.range = range;
        this.id = id;
        id++;
    }

    public void setRange(int y)
    {
        this.range = range;
    }

    public int getRange()
    {
        return this.range;
    }

    public void arPrint()
    {
        System.out.println("this is: " + this.name + "\n it`s weight:" + this.weight + "\n it`s range: " + this.range +
                "\n with " + this.damage + "damage \n it`s id: " + this.id + ".");
    }
}