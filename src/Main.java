import Items.Armor;
import Items.Weapon;
import Units.Archer;
import Units.Unit;
import Units.Swordman;

public class Main
{
        public void giveArmor(Armor object, Armor object3, Swordman object1, Archer object2)
        {
                String name = object.getName();
                String name1 = object1.getUnitName();
                String name2 = object2.getUnitName();
                String name3 = object3.getName();
                if (name == "Iron" && name1 == "Swordman")
                {
                        object1.health = object1.health + object.health;
                        object1.speed = object1.speed - object.weight;
                }
                if (name3 == "Leather" && name2 == "Archer")
                {
                        object2.health = object2.health + object3.health;
                        object2.speed = object2.speed - object3.weight;
                }
                else
                {
                        System.out.println("Error");
                }
        }

        public void giveWeapon(Weapon object, Weapon object3, Swordman object1, Archer object2)
        {
                String name = object.getName();
                String name1 = object1.getUnitName();
                String name2 = object2.getUnitName();
                String name3 = object3.getName();
                if (name == "Sword" && name1 == "Swordman")
                {
                        object1.damage = object1.damage + object.damage;
                        object1.speed = object1.speed - object.weight;
                }
                if (name3 == "Bow" && name2 == "Archer")
                {
                        object2.damage = object2.damage + object3.damage;
                        object2.speed = object2.speed - object3.weight;
                }
                else
                {
                        System.out.println("Error");
                }
        }

        public void main(String[] args)
        {
                Weapon sword = new Weapon("Sword", 10, 5, 1);
                Weapon bow = new Weapon("Bow", 5, 3, 7);
                Armor heavyarmor = new Armor("Iron", 100, 10);
                Armor lightarmor = new Armor("Leather", 50, 5);
                Swordman swordman = new Swordman("Swordman", 30, 10, 50);
                Archer archer = new Archer("Archer", 30, 10, 50);
                giveArmor(heavyarmor, lightarmor, swordman, archer);
                giveWeapon(sword, bow, swordman, archer);
        }
}