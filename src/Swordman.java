//TODO: Реализовать несколько классов воинов через наследование от класса Unit
//TODO: Добавить поля класса "Предмет" (см. TODO в Weapon)
//TODO: Сделать строки полями-константами и через enum
//TODO: Больше информации в print, разбить на несколько строк
//TODO: Добавить метод takeDamage, отнимающий броню и хп
//TODO: Если хватает фантазии, желательно больше характеристик
public class Swordman
{
    private int armor, damage, id;
    private String civ;
    private static int i = 0;

    public Swordman (String civ, int armor, int damage, int moves)
    {
        this.armor = armor;
        this.damage = damage;
        this.id = id;
        id++;
    }

    public void Print()
    {
        System.out.println(  this.armor + this.damage + this.id);
    }

    public void equipWeapon(Weapon item)
    {
        if ((item.getName() == "Sword"))
        {
            this.damage += item.getDamage();
        }
        else {
            System.out.println("Error");
        }
    }

    public void equipArmor(Armor item)
    {
        if (item.getName() == "Shield")
        {
            this.armor += item.getHealth();
        }
        else
            {
            System.out.println("Error");
        }
    }

    public int getId()
    {
        return this.id;
    }

    public int getDamage()
    {
        return this.damage;
    }

    public void setDamage(int a)
    {
        this.damage = a;
    }

    public int getArmor()
    {
        return this.armor;
    }

    public void setArmor(int a)
    {
        this.armor = a;
    }

}
